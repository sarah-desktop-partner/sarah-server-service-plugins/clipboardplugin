/*************************************************************************
*    Sarah - Desktop Partner's Plugins
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**************************************************************************/

#include "PluginManager.h"
#include "ClipboardPlugin.h"


IPlugin *ClipboardPluginManager::createPluginInstance(std::map<std::string, std::string> &params) {
    return new ClipboardPlugin();
}

void ClipboardPluginManager::destroyPluginInstance(IPlugin *plugin) {
    auto *s=(ClipboardPlugin*)plugin;
    s->Remove();
}

PLUGIN_ID ClipboardPluginManager::getPluginID() {
    return 4523519;
}
