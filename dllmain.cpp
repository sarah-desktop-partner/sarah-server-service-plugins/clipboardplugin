/*************************************************************************
*    Sarah - Desktop Partner's Plugins
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**************************************************************************/

//
// Created by ebrahim on 12/17/19.
//


#include <Manager.h>
#include "PluginManager.h"

#ifdef _WIN32
#define DLLEXPORT extern "C" __declspec (dllexport)
#else
#define DLLEXPORT extern "C"
#endif
//#define LoadActions LoadActions_clipboardPlugin
//#define UnloadActions UnloadActions_clipboardPlugin

IManager* manager;

DLLEXPORT void LoadActions(IManager* manager){
    ::manager = manager;
    auto *plg_manager = new ClipboardPluginManager();
    manager->registerPlugin(plg_manager,plg_manager->getPluginID());
}

DLLEXPORT void UnloadActions(){

}