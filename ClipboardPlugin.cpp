/*************************************************************************
*    Sarah - Desktop Partner's Plugins
*    Copyright (C) 2019-2019 Ebrahim Karimi
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <https://www.gnu.org/licenses/>.
**************************************************************************/

//
// Created by ebrahim on 12/17/19.
//

#include "ClipboardPlugin.h"

#include "../../Interface/Manager.h"
#include <libclipboard.h>
#include "json/json.h"

extern IManager *manager;

void ClipboardPlugin::onPacketReceived(ISarahPacket *packet) {
    Json::Reader reader;
    Json::Value rootVal;
    reader.parse(packet->toString(), rootVal);
    if (rootVal.isObject())
        if (!rootVal["msg"].isNull()) {
            if (rootVal["msg"].isObject()) {
                if (!rootVal["msg"]["content"].isNull()) {
                    clipboard_c *cb = clipboard_new(NULL);
                    if (cb == NULL) {
                        manager->Log("Couldn't create new clipboard instance in libclipboardPlugin, returning",
                                     LL_ERROR);
                        return;
                    }
                    std::string data = rootVal["msg"]["content"].asString();
                    clipboard_set_text_ex(cb, data.c_str(), static_cast<int>(data.size()), LCB_CLIPBOARD);
                    manager->Log("Clipboard Plugin : Content Changed to : " + rootVal["msg"]["content"].asString());
                }
            }
        }
}

std::vector<std::string> ClipboardPlugin::getHandledPacketTypes() {
    return handled_packets;
}
